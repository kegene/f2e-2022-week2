# F2E 2022 / PDF 簽核功能
`vue3` `typescript` `axios`

![](./readme-assets/000.jpg)

## 簡介

提供用戶可在線上替他的PDF文件附上電子簽名。

## 項目資訊

- Vue 3
- Pinia
- Naive UI
- jsPDF
- PDFjs
- Axios
- Tailwind 3
- Vite

## 設計參考來源：
[間諜吐司 - 今晚，我想來點點簽｜2022.thef2e.com](https://2022.thef2e.com/users/12061579703807156568)
## 功能開發


- [X] 提供簽名檔案的手寫功能
   - [X] 可更換顏色
   - [X] 兩種筆頭模式：原子筆(細體+不透明色) & 螢光筆(粗體+透明色)
   - [X] 切換到螢光筆時，透明度主動調低
   - [X] 可重置、返回步驟
- [X] 提供簽名檔案的上傳功能
   - [X] 上傳後於畫面中提前預覽
- [X] 提供PDF文件的上傳功能
- [ ] 提供PDF文件的編輯功能
   - [ ] 簽名檔案 插入 PDF文件中，並可進行移動、旋轉、放大縮小
   - [ ] PDF頁面切換，並顯示頁碼
