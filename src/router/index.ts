import { createRouter, createWebHistory } from 'vue-router'
import { usePdfStore } from '@/stores/PdfStore';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/sign',
      name: 'sign',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('@/views/SignView.vue')
    },
    {
      path: '/docs',
      name: 'docs',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('@/views/DocsView.vue')
    },
    {
      path: '/docs/:id',
      name: 'docs-edit',
      component: () => import('@/views/docs-edit'),
      beforeEnter: (to,from,next) => {
        const pdfStore = usePdfStore()
        if (!to.params.id || to.params.id !== pdfStore.file.id) return next('/');
        next()
      }
    },
    {
      path: '/setting',
      name: 'setting',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('@/views/SettingView.vue')
    },
    {
      path: '/',
      redirect: '/sign'
    }
  ]
})

export default router
