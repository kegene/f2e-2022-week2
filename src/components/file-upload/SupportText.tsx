type Props = {
    texts: string[];
}

export default (props: Props) => {
  return (
    <div class="uppercase text-xs">
      <span class="align-middle">支援檔案類型：</span>
      {
        props.texts.map((text, i)=> (
          <span key={i} class={`font-number align-middle`}>
            { i === 0 ? undefined : <span class="inline-block mb-2 w-1 h-1 bg-[color:#B0C3CA] rounded-full mx-2"></span>}
            <span class="align-super">{text}</span>
          </span>
        ))
      }
    </div>
  )
}