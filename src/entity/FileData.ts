export type IFileData = {
    id: string;
    src: string;
    name: string;
    type: string;
    file: File;
}
export class FileData {
  public id: IFileData['id'] = '';
  public src: IFileData['name'] = '';
  public name: IFileData['src'] = '';
  public type: IFileData['type'] = '';
  public file: IFileData['file'] = new File([],'');
  constructor(data?: IFileData) {
    this.set(data)
  }
  public clear() {
    this.id = '';
    this.src = '';
    this.name = '';
    this.type = '';
    this.file = new File([], '')
  }
  public get() {
    const{id,name,src,type,file} = this
    return {id,name,src,type,file}
  }
  set(data?: Partial<IFileData>) {
    if (!data) return;
    this.id = data.id || this.id;
    this.src = data.src || this.src;
    this.name = data.name || this.name;
    this.type = data.type || this.type;
    this.file = data.file || this.file;
  }
}