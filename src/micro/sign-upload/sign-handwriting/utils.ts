export const resolveHighlighterRGBColor = (color: string, opacity: string = '0.015') => {
  // console.log(color, color.replace(/rgba\((.*)(, ?\d{0,}\.?\d+)\)/, `rgba($1,${opacity})`))
  return color.replace(/rgba\((.*)(, ?\d{0,}\.?\d+)\)/, `rgba($1,${opacity})`);
}