import type {Ref} from 'vue';
import { ref } from 'vue';
import type * as Draw from './useDraw.d';
export {Draw};

export enum DrawPenWidth {
  PEN = 0.5,
  HIGHLIGHTER = 10
}

const getTrueObject = <T extends object, K extends keyof T>(object?: T) => {
  if (!object) return {} as T
  const keys = Object.keys(object) as K[];
  return keys.reduce((o, key)=>{
    const value = object[key];
    if (value) {
      o[key] = value
    }
    return o
  },{} as T)
}

export const useDraw = (canvas: Ref<HTMLCanvasElement | null>, options?: Draw.Option): Draw.ToReturnType => {
  if (!canvas.value) throw new Error('can find canvas');
  const ctx = ref<CanvasRenderingContext2D | null>(null);
  const isDrawing = ref(false)
  const defaultOptions: Required<Draw.Option> = {
    width: canvas.value.width,
    height: canvas.value.height,
    lineWidth: 1,
    shadowColor: '#000000',
    lineCap: 'round',
    lineJoin: 'round',
  }
  const _options = ref<Required<Draw.Option>>({
    ...defaultOptions,
    ...getTrueObject(options),
  });

  const getPosition: Draw.GetPosition = {
    mouse: (canvas, evt) => {
      const rect = canvas.getBoundingClientRect();
      return {
        x: evt.clientX - rect.left,
        y: evt.clientY - rect.top
      };
    },
    touch: (canvas, evt) => {
      const rect = canvas.getBoundingClientRect();
      return {
        x: evt.touches[0].clientX - rect.left,
        y: evt.touches[0].clientY - rect.top
      };
    }
  }
  const drawStart = <T extends Draw.EventName>(key: T, event: Draw.EventType<T>) => {
    if (!canvas.value || !ctx.value) return;
    isDrawing.value = true
    const pos = getPosition[key](canvas.value, event);
    ctx.value.beginPath();
    ctx.value.moveTo(pos.x, pos.y);
    event.preventDefault();
  }
  const drawEnter = <T extends Draw.EventName>(key: T, event: Draw.EventType<T>) => {
    if (!isDrawing.value) return;
    if (!canvas.value || !ctx.value) return;
    const pos = getPosition[key](canvas.value, event);
    ctx.value.lineWidth = _options.value.lineWidth;
    ctx.value.lineCap = _options.value.lineCap
    ctx.value.lineJoin = _options.value.lineJoin;
    ctx.value.shadowBlur = 1; // 邊緣銳利度
    ctx.value.strokeStyle = _options.value.shadowColor; // 陰影色
    ctx.value.shadowColor = _options.value.shadowColor; // 陰影邊緣色
    ctx.value.lineTo(pos.x, pos.y);
    ctx.value.stroke();
  }
  const drawEnd = <T extends Draw.EventName>(event: Draw.EventType<T>) => {
    isDrawing.value = false
  };
  const mouse: Draw.DrawHandler<'mouse'> = {
    drawStart: (evt: MouseEvent) => drawStart('mouse', evt),
    drawEnter: (evt: MouseEvent) => drawEnter('mouse', evt),
    drawEnd: (evt: MouseEvent) => drawEnd(evt),
  };
  const touch: Draw.DrawHandler<'touch'> = {
    drawStart: (evt: TouchEvent) => drawStart('touch', evt),
    drawEnter: (evt: TouchEvent) => drawEnter('touch', evt),
    drawEnd: (evt: TouchEvent) => drawEnd(evt),
  };
  const clearRect = (options?: Partial<Draw.Option>): void => {
    if (!ctx.value) return;
    if (!canvas.value) return;
    const width = options?.width ?? _options.value.width;
    const height = options?.height ?? _options.value.width;
    ctx.value.clearRect(0, 0, width, height);
  };
  const convertToImage = (): string | undefined => {
    if (!canvas.value) return;
    return canvas.value.toDataURL();
  };
  const putOptions = (options: Draw.Option): void => {
    _options.value = {..._options.value, ...options}
  }
  const initCtx = (context?: CanvasRenderingContext2D): void => {
    if (!canvas.value) return;
    ctx.value = context || canvas.value.getContext('2d')
    canvas.value.addEventListener('mousedown', mouse.drawStart)
    canvas.value.addEventListener('mousemove', mouse.drawEnter)
    canvas.value.addEventListener('mouseup', mouse.drawEnd)
    canvas.value.addEventListener('mouseleave', mouse.drawEnd)
    canvas.value.addEventListener('touchstart', touch.drawStart)
    canvas.value.addEventListener('touchmove', touch.drawEnter)
    canvas.value.addEventListener('touchend', touch.drawEnd)
  }
  const unmountCtx = (): void => {
    if (!canvas.value) return;
    canvas.value.removeEventListener('mousedown', mouse.drawStart)
    canvas.value.removeEventListener('mousemove', mouse.drawEnter)
    canvas.value.removeEventListener('mouseup', mouse.drawEnd)
    canvas.value.removeEventListener('mouseleave', mouse.drawEnd)
    canvas.value.removeEventListener('touchstart', touch.drawStart)
    canvas.value.removeEventListener('touchmove', touch.drawEnter)
    canvas.value.removeEventListener('touchend', touch.drawEnd)
  }
  return {
    initCtx,
    unmountCtx,
    putOptions,
    clearRect,
    convertToImage,
  }
}
