import type {Options} from 'signature_pad'
import type { Option, EmitAction } from './SingToolBar.vue'
import { DrawPenWidth } from './useDraw'

import IconPen from '@/components/icons/IconPen.vue'
import IconPenHighlighter from '@/components/icons/IconPenHighlighter.vue';
import IconReset from '@/components/icons/IconReset.vue';
import IconRedo from '@/components/icons/IconRedo.vue';
import IconUndo from '@/components/icons/IconUndo.vue';

export const options: Option<Options>[] = [
  {
    prop: 'color-picker',
    value: '#000000'
  },
  {
    icon: IconPen,
    prop: 'pen-base',
    value: {
      minWidth: DrawPenWidth.PEN,
      maxWidth: DrawPenWidth.PEN + 2,
    }
  },
  {
    icon: IconPenHighlighter,
    prop: 'pen-highlighter',
    value: {
      minWidth: DrawPenWidth.HIGHLIGHTER,
      maxWidth: DrawPenWidth.HIGHLIGHTER,
    }
  },
];
export const actions: EmitAction[] = [
  {
    icon: IconRedo,
    prop: 'redo'
  },
  {
    icon: IconUndo,
    prop: 'undo'
  },
  {
    icon: IconReset,
    prop: 'clear'
  }
]