export type EventName = 'mouse' | 'touch';
export type EventType<T> = T extends 'mouse' ? MouseEvent : TouchEvent;
export type DrawHandler<T extends EventName> = {
  drawStart: (event: EventType<T>) => void;
  drawEnter: (event: EventType<T>) => void;
  drawEnd: (event: EventType<T>) => void;
}
export type GetPosition = {
    [K in EventName]: (c: HTMLCanvasElement, e: EventType<K>) => {x:number, y:number}
}
export interface Option {
    width: number;
    height: number;
    /** 線的粗度 */
    lineWidth?: number;
    /** 畫筆陰影色 */
    shadowColor?: string;
    /** 下筆跟起筆的頭的樣式 */
    lineCap?: CanvasLineCap;
    /** 線交叉時的樣式 */
    lineJoin?: CanvasLineJoin;
}
export interface ToReturnType {
  putOptions: (options: DrawOption) => void;
  clearRect: (options?: Partial<DrawOption>) => void;
  convertToImage: () => string | undefined;
  initCtx: (context?: CanvasRenderingContext2D) => void;
  unmountCtx: () => void;
}