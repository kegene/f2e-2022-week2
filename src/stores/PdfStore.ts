import {reactive} from 'vue'
import { defineStore } from 'pinia'

interface PdfFile {
  id: string;
  src: string;
  name: string;
  type: string;
  file: File;
}

export const usePdfStore = defineStore('pdf', () => {
  const file = reactive<PdfFile>({
    name: '',
    id: '',
    src: '',
    type: '',
    file: new File([],'')
  });
  const get = () => file;
  const set = (data: PdfFile) => Object.assign(file, data)

  return { file, get, set }
})
