import {reactive} from 'vue'
import { defineStore } from 'pinia'

interface Sign {
    id: string;
    src: string;
    name: string;
}

export const useSignStore = defineStore('sign', () => {
  const signs = reactive<Sign[]>([])
  const getSign = (id: string) => signs.find((sign) => sign.id === id);
  const setSign = (sign: Sign) => signs.push(sign); 

  return { signs, setSign, getSign }
})
