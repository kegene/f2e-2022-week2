import type {VNode} from 'vue';
import {h} from 'vue';
import type {AlertProps, MessageRenderMessage} from 'naive-ui';
import {NAlert, useMessage as NuseMessage} from 'naive-ui';

type Type = AlertProps['type']

type Props = Parameters<MessageRenderMessage>[0]

type Content = string | VNode;

const getType = (type: 'loading' | Type): Type => type === 'loading' ? 'default' : type
const getRender = (props: Props, title: string, content: Content) => h(NAlert,
  {
    title: title,
    type: getType(props.type)
  },
  {
    default: () => content
  }
)
export const useMessage = () => {
  const {error, success, warning, info} = NuseMessage()
  return {
    error(content: Content, options = {duration: 1000 * 5}) {
      error('',{...options,render: (props) => getRender(props, '', content)})
      return false;
    },
    success(content: Content, options = {duration: 1000 * 5}) {
      success('',{...options,render: (props) => getRender(props, '', content)})
      return false;
    },
    warning(content: Content, options = {duration: 1000 * 5}) {
      warning('',{...options,render: (props) => getRender(props, '', content)})
      return false;
    },
    info(content: Content, options = {duration: 1000 * 5}) {
      info('',{...options,render: (props) => getRender(props, '', content)})
      return false;
    },
  }
}