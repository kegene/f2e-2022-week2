export const colorToHex = (color: string) => {
  if (!document) return color;
  const ctx = document.createElement('canvas').getContext('2d') as CanvasRenderingContext2D;
  ctx.fillStyle = color;
  return ctx.fillStyle;
}
export const colorToRGBA = (color: string) => {
  const d = {
    str: '',
    ary: new Uint8ClampedArray()
  }
  const cvs = document.createElement('canvas');
  cvs.height = 1;
  cvs.width = 1;
  const ctx = cvs.getContext('2d');
  if (!ctx) return d;
  ctx.fillStyle = color;
  ctx.fillRect(0, 0, 1, 1);
  const ary = ctx.getImageData(0, 0, 1, 1).data;
  d.str = `rgba(${ary.join(',')})`;
  d.ary = ary;
  return d
}