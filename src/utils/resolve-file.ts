type FileReaderOnEvent = Pick<FileReader, 'onloadend'|'onloadstart'|'onprogress'>

const toBase64 = (file: File, event?: Partial<FileReaderOnEvent>): Promise<FileReader['result']> => new Promise((resolve, reject) => {
  const reader = new FileReader();
  reader.onload = () => resolve(reader.result);
  reader.onerror = error => reject(error);

  if (event) {
    const keys = Object.keys(event) as (keyof typeof event)[];
    for (const key of keys) {
      reader[key] = event[key] ?? reader[key];
    }
  }

  reader.readAsDataURL(file);

});

const toBuffer = (file: File, event?: Partial<FileReaderOnEvent>): Promise<FileReader['result']> => new Promise((resolve, reject) => {
  const reader = new FileReader();
  reader.onload = () => resolve(reader.result);
  reader.onerror = error => reject(error);

  if (event) {
    const keys = Object.keys(event) as (keyof typeof event)[];
    for (const key of keys) {
      reader[key] = event[key] ?? reader[key];
    }
  }

  reader.readAsArrayBuffer(file)

});

const toBlob = (data: any, options: BlobPropertyBag = {type: 'application/json'}) => new Blob([JSON.stringify(data)], options)

const isFileType = (types:string[], file?: File) => {
  if (!file) return false;
  return types.includes(file.type)
}
/**
 * 是否超出大小限制
 * @param fileSize bit
 * @param maxSize mb
 */
const isWithinSize = (fileSize: number, withinSize: number) => {
  const fileSizeToMb = fileSize / 1024 / 1024;
  return fileSizeToMb < withinSize;
}
export {
  toBase64,
  toBlob,
  isFileType,
  isWithinSize,
  toBuffer
}