/*eslint no-undef: "off"*/
/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{css,vue,ts,tsx,js,jsx}"
  ],
  theme: {
    extend: {
    
    },
    screens: {
      sm: '768px',
      md: '1024px',
      lg: '1280px',
      xl: '1536px',
      '2xl': '1920px',
    },
    colors: ({ colors }) => ({
      inherit: colors.inherit,
      current: colors.current,
      transparent: colors.transparent,
      black: '#2E2E2E',
      white: colors.white,
      slate: colors.slate,
      gray: colors.gray,
      zinc: colors.zinc,
      neutral: colors.neutral,
      stone: colors.stone,
      red: colors.red,
      orange: colors.orange,
      amber: colors.amber,
      yellow: colors.yellow,
      lime: colors.lime,
      green: colors.green,
      emerald: colors.emerald,
      teal: colors.teal,
      cyan: colors.cyan,
      sky: colors.sky,
      blue: colors.blue,
      indigo: colors.indigo,
      violet: colors.violet,
      purple: colors.purple,
      fuchsia: colors.fuchsia,
      pink: colors.pink,
      rose: colors.rose,
      primary: '#4F61E8',
      'primary-dark': '#1E2F61',
      'primary-deep': '#2E41CE',
      'primary-green': '#DDF0F4',
      'primary-pale': '#EEF7F9',
      'primary-light': '#F2FDFF',
      secondary: '#6DA9B7',
      alert: '#EF7054'
    }),
    fontFamily: ({fontFamily}) => {
      const sans = [
        'Raleway',
        'Noto Sans TC',
        'ui-sans-serif',
        'system-ui',
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        '"Noto Sans"',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
        '"Noto Color Emoji"',
      ]
      const number = sans.slice(1, sans.length);
      const notoTC = sans.slice(1, sans.length);

      return {
        ...fontFamily,
        sans,
        number,
        notoTC
      }},
    fontSize: {
      xs: ['0.75rem', { lineHeight: '1.6' }],
      sm: ['0.93rem', { lineHeight: '1.6' }],
      base: ['1rem', { lineHeight: '1.6' }],
      lg: ['1.075rem', { lineHeight: '1.6' }],
      xl: ['1.145rem', { lineHeight: '1.6' }],
      '2xl': ['1.5rem', { lineHeight: '2rem' }],
      '3xl': ['1.875rem', { lineHeight: '2.25rem' }],
      '4xl': ['2.25rem', { lineHeight: '2.5rem' }],
      '5xl': ['3rem', { lineHeight: '1' }],
      '6xl': ['3.75rem', { lineHeight: '1' }],
      '7xl': ['4.5rem', { lineHeight: '1' }],
      '8xl': ['6rem', { lineHeight: '1' }],
      '9xl': ['8rem', { lineHeight: '1' }],
    },
    boxShadow: {
      'base': '0px 2px 20px -4px rgba(205, 221, 225, 1)',
      'base-primary': '0px 2px 20px -4px rgba(23, 35, 122, 1)',
      'base-secondary':  '0px 2px 20px -8px rgba(109, 169, 183, 1)',
      'base-alert': '0px 2px 20px -4px rgba(206, 88, 62, 1)',
      inner: 'inset 0 2px 4px 0 rgb(0 0 0 / 0.05)',
      none: 'none',
    },
  },
  plugins: [],
}
